class Disruption {
  
  float OBJECT_SIZE;
  
  boolean isPressed;
  
  float x,y;
  
  float _x,_y;
  
  Disruption(float initX, float initY) {
    
   OBJECT_SIZE = 40; 

   isPressed = false;  
   
   x = initX;
   
   y = initY;
    
     
  }
  
  void update(boolean isPressed,float xPos,float yPos) {
    
    ellipseMode(CENTER);
    _x = xPos-width/2;
    _y = yPos-height/2;
    fill(0,255,0,100);
    noStroke();

    
    if (isPressed && isMouseWithin(_x,_y)) {
      
     x = _x;
     y = _y;
      
    }
    ellipse(x,y,OBJECT_SIZE,OBJECT_SIZE);
    fill(0,255,0,255);
    ellipse(x,y,5,5);

    
    
  }
  
  boolean isMouseWithin(float _x, float _y) {
    
    if ((_x > x - (OBJECT_SIZE/2) && _x < x + (OBJECT_SIZE/2)) && (_y > y - (OBJECT_SIZE/2) && _y < y + (OBJECT_SIZE/2))) {
      return true;
    } else {
      return false;
    }
    
  }
  
  

  
  
  
  
  
  
  
  
  
  
  
  
}
