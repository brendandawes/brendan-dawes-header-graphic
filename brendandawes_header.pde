/*

Brendan Dawes Header Generator
Used to generate the header image on brendandawes.com

Requires Geomerative, Physics and Toxiclibs libraries

*/

import geomerative.*;
import processing.opengl.*;
import traer.physics.*;
import processing.pdf.*;
import java.util.Iterator;

import toxi.color.*;
import toxi.geom.*;
import toxi.math.*;
import toxi.util.datatypes.*;


final int NUMBER_OF_DISRUPTIONS = 5;
final int CIRCLE = 1;
final int RECTANGLE = 2;
final color BACKGROUND_COLOR = color(249, 249, 249);
final float SCALE_FACTOR = 1.2;
final int REPEL_FACTOR = -50000;
final int GRID_SIZE = 2;


RShape grp;
RPoint[] points;
RShape shp;

ParticleSystem physics;
Particle[] particles, fixedParticles;
Particle mouse;
Particle disruptionParticle;
Particle particle;

boolean record;
boolean isPressed;

ArrayList nodes;

ArrayList disruptions;

ArrayList disruptionParticles;

ColorList list;

int sampleRate = 2;



void setup(){
  
  isPressed = false;

  nodes = new ArrayList();
  
  disruptions = new ArrayList();
  
  disruptionParticles = new ArrayList();

  size(640,640);
  ellipseMode(CENTER);
  rectMode(CENTER);
  background(BACKGROUND_COLOR);
  smooth();

  RG.init(this);
  shp = RG.loadShape("brendandawes_strangelove_logo3.svg");
  shp = RG.centerIn(shp, g);
  points = shp.getPoints();

  physics = new ParticleSystem(0,0.05);
  fixedParticles = new Particle[points.length];
  particles = new Particle[points.length];

  for (int c = 0; c < NUMBER_OF_DISRUPTIONS ; c++) {

    float xPos = (c*width/NUMBER_OF_DISRUPTIONS)-width/2+width/NUMBER_OF_DISRUPTIONS/2;
    Disruption d = new Disruption(xPos,10);
    disruptions.add(d);
    disruptionParticle = physics.makeParticle( 1.0, xPos, 1000, 0 );
    disruptionParticles.add(disruptionParticle);
    
  }

 

  for ( int i = 0; i < particles.length; ++i ) {

    fixedParticles[i] = physics.makeParticle( 1.0, points[i].x/SCALE_FACTOR, points[i].y/SCALE_FACTOR, 0);
    fixedParticles[i].makeFixed(); 
    particles[i] = physics.makeParticle( 1.0, points[i].x/SCALE_FACTOR, points[i].y/SCALE_FACTOR, 0);
    physics.makeSpring( fixedParticles[i],  particles[i], 0.1,0.1, 1 );

    for (int c = 0; c < disruptionParticles.size(); c++) {
      Particle disruptionParticle = (Particle) disruptionParticles.get(c);
      physics.makeAttraction( disruptionParticle,  particles[i], REPEL_FACTOR, 1 ); 
    
    }
  }
  
  ColorTheme theme = new ColorTheme("winter");
  //theme.addRange("warm yellow", 0.5);
  theme.addRange("warm ivory", 0.5); 
  theme.addRange("warm teal", 0.5); 
  theme.addRange("soft pink", 0.5); 
  theme.addRange("soft red", 0.5);   
  list =  theme.getColors(physics.numberOfParticles());
  
  float noiseInc = 0;

  for ( int i = 1; i < physics.numberOfParticles(); i++ ) {

    particle = physics.getParticle( i );

    if (particle.isFixed() == false) {
      color col = list.get(i).toARGB();
      int alp = 100;
      int nSize = int(noise(noiseInc)*20);
      Node n = new Node(particle.position().x(), particle.position().y(),nSize,nSize,col,alp,RECTANGLE);
      nodes.add(n);
      noiseInc += 0.1;
    }

  }

}

void draw(){
 
  
  if (record) {
    beginRecord(PDF, "frame-####.pdf"); 
  }

  background(BACKGROUND_COLOR);
  translate(width/2, height/2);

  
  for (int c=0; c < disruptions.size(); c++) {
    
    Disruption d = (Disruption) disruptions.get(c);

      if (!record) {
        d.update(isPressed,mouseX,mouseY);
      }

    Particle disruptionParticle = (Particle) disruptionParticles.get(c);
    disruptionParticle.moveTo(d.x,d.y,0);
    
  }

  physics.tick();
 
  
 
  if (isPressed) {
    sampleRate = 10;
  } else {
    sampleRate = 2; 
  }

  int counter = 0;

  for ( int i = 1; i < physics.numberOfParticles()/sampleRate; i++ ) {
    particle = physics.getParticle( i*sampleRate );

    if (particle.isFixed() == false) {

      Node node = (Node) nodes.get(counter);
      float snappedXpos = floor( particle.position().x() / GRID_SIZE) * GRID_SIZE;
      float snappedYpos = floor( particle.position().y() / GRID_SIZE) * GRID_SIZE;
      node.update(snappedXpos, snappedYpos);
      counter ++;

    }

  }
  

  if (record) {

    endRecord();
    record = false;

  }

 
}

void mousePressed() {

  isPressed = true;

}

void mouseReleased() {
  
  isPressed = false;

}

void keyPressed() {

  record = true;
  
}





