# Brendan Dawes Header Graphic Creator

This is the Processing code used to create the header graphic on [brendandawes.com](http://brendandawes.com/).

It uses a physics library and an SVG library to create the image, together with Toxiclibs for colour.

- Drag the green nodes around to disrupt the shape
- Press any key to save the image as a PDF

Load in your own SVG shape and play around.



