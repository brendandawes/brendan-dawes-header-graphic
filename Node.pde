class Node {

  color nodeColor;

  int nodeWidth, nodeHeight;

  int nodeAlpha;

  float nodeRandom;

  int nodeType;

  Node(float x, float y, int w, int h, color col, int alp, int shapeType) {

    nodeColor = col;

    nodeWidth = w;

    nodeHeight = h;

    nodeAlpha = alp;

    nodeRandom = random(5) - random(3);

    nodeType = shapeType;

  }

  void update(float x, float y) {

    noFill();
    strokeWeight(1);
    stroke(nodeColor, 255);
    switch(nodeType) {
    case 1: 
      ellipse(x, y, nodeWidth, nodeHeight);
      break;
    case 2: 
      rect(x, y, nodeWidth, nodeHeight);
      break;
    default:
      break;
    }


    stroke(nodeColor, 50);
    //line(x, height+1000, x, y);
    
  }
}

